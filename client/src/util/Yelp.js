const apiKey =
  "0ugZPTi1-D-Q_22gXTTzz9ar8YaMBSqwXnJtUUyO9Fn0nBb9h2YCQG5PEEpCeB9GqmTo68Ku-83wQL7BmqeYjccfZOMlTWcckzYH1N2ikVM_hnsLVdi0_CL2le-UXXYx";

const Yelp = {
  searchYelp(term, location, sortBy) {
    return fetch(
      `https://cors-anywhere.herokuapp.com/https://api.yelp.com/v3/businesses/search?term=${term}&location=${location}&sort_by=${sortBy}`,
      {
        headers: {
          Authorization: `Bearer ${apiKey}`
        }
      }
    )
      .then(Response => {
        return Response.json();
      })
      .then(jsonResponse => {
        if (jsonResponse.businesses) {
          return jsonResponse.businesses.map(business => {
            return {
                id: business.id,
                imageSrc: business.image_url,
                name: business.name,
                address: business.location.address1,
                city: business.location.city,
                state: business.location.state,
                zip: business.location.zip_code,
                category: business.categories[0].title,
                rating: business.rating,
                reviewCount: business.review_count
            };
          });
        }
      });
  }
};

export default Yelp;
